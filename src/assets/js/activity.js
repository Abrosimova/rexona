import appOptions from './settings';
import API from './api';
// import Rating from './rating';

export default {
  get(user, callback) {
    const that = this;
    that.userId = user.id_str;
//    API.deleteKeyFromDB(`${appOptions.provider}-activity-user_1000004285735`);
//    API.deleteKeyFromDB(`${appOptions.provider}-activity-user_1000004285741`);
    API.getKeyFromDB({ key: `${appOptions.provider}-activity-user_${user.id_str}` }, (data) => {
      /* eslint-disable */
//      data = null;
      /* eslint-enable */
      if (data) {
        that.model = JSON.parse(data.Value);
        that.model.schedule = that.model.schedule || 0;
        that.model.likedSchedule = that.model.likedSchedule || [];
        if (typeof callback === 'function') {
          callback(that.model);
        }
      } else {
        that.model = {
          user,
          comment: 0,
          pers: 0,
          schedule: 0,
          likedSchedule: [],
          liked: [],
          reposted: [],
        };
        that.set();
        if (typeof callback === 'function') {
          callback(that.model);
        }
      }
    });
  },
  set(callback) {
    const that = this;
    API.addKeyToDB({
      label: `${appOptions.provider}-activity`,
      key: `${appOptions.provider}-activity-user_${this.userId}`,
      value: JSON.stringify(that.model),
    }, () => {
      if (typeof callback === 'function') {
        callback();
      }
    });
  },
};
