let isMobile;

if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
|| navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
|| navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
|| navigator.userAgent.match(/Windows Phone/i)) {
  isMobile = true;
} else {
  isMobile = false;
}

const locationUrl = document.location.href;

export default (locationUrl.indexOf('school.mosreg') > -1) ?
{
  authUrl: 'https://login.school.mosreg.ru/oauth2',
  grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
  scope: 'CommonInfo,Wall',
  clientId: 'aeb1c499297e401988ae815d92079c30',
  redirectUrl: `${window.location.href}/?auth=true`,
  provider: 'mosreg-rexona',
  api: 'https://api.school.mosreg.ru/v1/',
  isMobile,
  userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
  cdnPath: 'https://ad.csdnevnik.ru/special/staging/rexona/img/',
  adminIds: [],
}
:
{
  authUrl: 'https://login.dnevnik.ru/oauth2',
  grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
  scope: 'CommonInfo,Wall',
  clientId: '2470cf7ff452456280372ef613098f70',
  redirectUrl: `${window.location.href}/?auth=true`,
  provider: 'rexona',
  api: 'https://api.dnevnik.ru/v1/',
  isMobile,
  userLink: 'https://dnevnik.ru/user/user.aspx?user=',
  cdnPath: 'https://ad.csdnevnik.ru/special/staging/rexona/img/',
  adminIds: ['1000006315838', '1000003565717', '1000004681017'],
};
