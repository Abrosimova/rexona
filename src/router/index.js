import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import Contest from '@/components/Contest';
import AdviceList from '@/components/AdviceList';
import Game from '@/components/Game';
import Schedule from '@/components/Schedule';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '*',
      name: 'Main',
      component: Main,
    },
    {
      path: '/contest',
      name: 'Contest',
      component: Contest,
    },
    {
      path: '/advice',
      name: 'AdviceList',
      component: AdviceList,
    },
    {
      path: '/game',
      name: 'Game',
      component: Game,
    },
    {
      path: '/schedule',
      name: 'Schedule',
      component: Schedule,
    },
  ],
});
