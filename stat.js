API.getKeysFromDB({
  label: appOptions.provider + '-activity',
  pageSize: 30,
  pageNumber: 1
}, function (data) {
  if (data.Paging.next) {
    API.getRecursivePage(data.Paging.next, function (data) {
      data = data.Keys;
      var count = {};
      var roles = {
        parent: 0,
        child: 0
      }
      var xhrCouner = 0;
      var userIds = [];
      
      for (var i = 0; i < data.length; i++) {
        var json = JSON.parse(data[i].Value);
        userIds.push(data[i].UserId);
        var counter = 0;
        for (var j = 0; j < json.lessons.length; j++) {
          if (json.lessons[j].video === 1 && json.lessons[j].task === 1) {
            counter++;
          }
        }
        if (count[counter] !== void 0) {
          count[counter]++;
        } else {
          count[counter] = 1;
        }
//        debugger
//        if (json.user.roles.indexOf('EduStudent') > -1) {
//          roles.child++;
//        } else if (json.user.roles.indexOf('EduParent') > -1) {
//          roles.parent++;
//        }
      }
      
      function recCallback () {
        xhrCouner++;
        if (xhrCouner === userIds.length - 1) {
          console.log(roles)
        } else {
          rec(xhrCouner, recCallback)
        }
      }
      
      rec(0, recCallback)
      
      function rec(i, callback) {
        setTimeout(function() {
          $.ajax({
            contentType: 'application/json',
            data: JSON.stringify({}),
            dataType: 'json',
            success: function(data) {
              // console.log(data);
              if (void 0 !== data) {
                if (data.indexOf('EduStudent') > -1) {
                  roles.child++;
                } else if (data.indexOf('EduStaff') > -1) {
                  roles.parent++;
                }
                callback()
              }
            },
            processData: false,
            type: 'GET',
            url: appOptions.api + 'users/' + userIds[i] + '/roles?access_token=' + API.token
          });
        }, 30);
      }
      
      console.log(count)
    }, data);
  }
  
});